
package melody.client;
import melody.common.MelodyOutput;
import com.platform.symphony.soam.*;


public class MySessionCallback extends SessionCallback
{
    //=========================================================================
    //  Constructor
    //=========================================================================

    public MySessionCallback(int tasksToReceive) 
    {
        m_tasksReceived = 0;
        m_exception = false;
        m_tasksToReceive = tasksToReceive;
    }

    //=========================================================================
    //  SessionCallback Interface Methods
    //=========================================================================

    /**
     * Invoked when an exception occurs within the scope of the given session.
     * Must be implemented by the application developer. 
     */
    public void onException(SoamException exception) throws SoamException
    {
        /********************************************************************
         * Although the Symphony API currently invokes this method with a
         * single callback thread, the number of threads used by the API to
         * invoke this method may increase in future revisions of the API.
         * Therefore, the developer should never assume that the invocation
         * of this method will be done serially and should always implement
         * this method in a thread-safe manner. 
         ********************************************************************/
        
        System.out.println("An exception occured on the callback :");
        System.out.println(exception.getMessage());
        setException(true);
    }

    /**
     * Invoked when a task response is ready.
     * Must be implemented by the application developer. 
     */
    public void onResponse(TaskOutputHandle output) throws SoamException
    {
        /********************************************************************
         * Although the Symphony API currently invokes this method with a
         * single callback thread, the number of threads used by the API to
         * invoke this method may increase in future revisions of the API.
         * Therefore, the developer should never assume that the invocation
         * of this method will be done serially and should always implement
         * this method in a thread-safe manner. 
         ********************************************************************/
        
        try
        {
            // check for success of task
            if (output.isSuccessful())
            {
                // get the message returned from the service
                MelodyOutput myOutput = new MelodyOutput();
                output.populateTaskOutput(myOutput);

                // display content of reply
                System.out.println("\nTask Succeeded [" +  output.getId() + "]");
                System.out.println("Your Internal ID was : " + myOutput.getId());
                System.out.println("Estimated runtime was recorded as : " + myOutput.getRunTime());
                System.out.println(myOutput.getString());
                byte[] data = myOutput.getData().buf();
                System.out.println("Data block size is: "+data.length);
                myOutput.releaseSoamDataBlock();
            }
            else
            {
                // get the exception associated with this task
                SoamException ex = output.getException();
                System.out.println("Task Not Successful :");
                System.out.println(ex.getMessage());
            }
        }
        catch (Exception exception)
        {
            System.out.println("Exception occured in onResponse() : ");
            System.out.println(exception.getMessage());
        }

        // Update counter used to synchronize the controlling thread 
        // with this callback object
        incrementTaskCount();
    }
    
    ////////////////////////////////////////////////////////////////////////
    // This method is invoked when a task sending status is available. It is
    // optional to be implemented by the application developer. To enable
    // this function, use flag RECEIVE_ASYNC_TASK_SENT_STATUS when creating a
    // session. 
    ////////////////////////////////////////////////////////////////////////
    public void onTaskSent(TaskInputHandle taskInput) throws SoamException
    {
        /********************************************************************
         * Although the Symphony API currently invokes this method with a
         * single callback thread, the number of threads used by the API to
         * invoke this method may increase in future revisions of the API.
         * Therefore, the developer should never assume that the invocation
         * of this method will be done serially and should always implement
         * this method in a thread-safe manner. 
         ********************************************************************/
        
        try
        {
            System.out.println("\n onTaskSent is invoked, the task state is [" + taskInput.getSubmissionState() + "]."); 
        }
        catch (Exception exception)
        {
            System.out.println("Exception occured in onTaskSent() : ");
            System.out.println(exception.getMessage());
        }
    }

    //=========================================================================
    //  Additional Public Methods
    //=========================================================================

    public boolean isDone()
    {
        if (m_exception)
        {
            return true;
        }
        return m_tasksReceived == m_tasksToReceive;
    }


    //=========================================================================
    //  Private Synchronized Helper Methods
    //=========================================================================

    private synchronized void incrementTaskCount()
    {
        m_tasksReceived++;
        if (m_tasksReceived == m_tasksToReceive)
        {
            this.notifyAll();
        }
    }

    private synchronized void setException(boolean exception)
    {
        m_exception = exception;
        this.notifyAll();
    }

    //=========================================================================
    //  Private Member Variables
    //=========================================================================

    private int m_tasksReceived;
    private boolean m_exception;
    private int m_tasksToReceive;
}
