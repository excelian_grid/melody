package melody.client;

import java.util.Random;
import com.platform.symphony.soam.*;
import melody.common.*;



class AsyncClient
{
    public static void main(String[] args) throws Exception
    {   
    	 // Set up application specific information to be supplied to Symphony
    	String appName = "MelodyApp-ClusterTest";
        int jobsToSubmit = 1;
    	int tasksToSend = 10;
    	String user = "Admin";
    	String password = "Admin";
    	String str_data_size = "1000";
    	String clusterUrl = "master_list://uat-gridmg01:7870";
 //   	String clusterUrl = "master_list://devcent:7870";
    	
    	Connection connection = null;
    	
    	try {    		
            
            if (args != null && args.length<5) {
            	System.out.println("Usage: Run MelodyClient.sh [number_of_sessions] [number_of_tasks] [user] [password]");
            	System.out.println("	[num_of_sessions] - The number of jobs to be subimitted");
            	System.out.println("	[number_of_tasks] - The number of tasks to be submitted in a service instance");
            	System.out.println("	[user] - The username of a client account that will submit the tasks");
            	System.out.println("	[password] - password for the client account");
            	System.out.println("	[random] or [size_of_data_chunk] - A size of an input data chunk. Type keyword random for random size or specify with number");
            	System.exit(1);
            }
            
            jobsToSubmit = Integer.parseInt(args[0]);
            tasksToSend = Integer.parseInt(args[1]);
            user = args[2];
            password = args[3];
            str_data_size = args[4];
            
            System.out.println(tasksToSend);
           
            // Initialize the API
            SoamFactory.initialize();
            
            
            // Set up application authentication information using the default security provider
            DefaultSecurityCallback securityCB = new DefaultSecurityCallback(user, password);
            
            connection = SoamFactory.connect(clusterUrl, appName, securityCB);
            
            while (jobsToSubmit!=0)
            {
	            submitServiceSession(appName, tasksToSend, connection, securityCB, jobsToSubmit, str_data_size);
	            jobsToSubmit--;
	            System.out.println(jobsToSubmit);
            }

    	} catch (Exception ex) {
        
        	// Report exception
        	System.out.println("Exception caught : ");
        	System.out.println(ex.toString());
        	ex.printStackTrace();
    
    	} finally {
            
    	  	// Connection close
            if (connection != null)
            {
                System.out.println("***** Connection not null ******");
            	connection.close();
            }

        // Uninitialize the API
        // This is the only means to ensure proper shutdown 
        // of the interaction between the client and the system.
        SoamFactory.uninitialize();
        System.out.println("All Done !!");
        }
    }

        public static void submitServiceSession(String appName, 
        										int tasksToSend,
        										Connection connection ,
        										DefaultSecurityCallback securityCB,
        										int jobsToSubmit,
        										String str_data_size)
        {   	
                try
                {
                    // Retrieve and print our connection ID
                    System.out.println("connection ID=" + connection.getId());
                    
                    MySessionCallback myCallback = new MySessionCallback(tasksToSend);
                    Session session = null;
                    int data_size;
                    int spinCpu;
                    
                    // Set up session attributes
                    SessionCreationAttributes attributes = new SessionCreationAttributes();
                    attributes.setSessionName(appName + "_Service_Session_" + jobsToSubmit);
                    attributes.setSessionType("ShortRunningTasks");
                    attributes.setSessionFlags(Session.PARTIAL_ASYNC);
                    attributes.setSessionCallback(myCallback);
                    // Create an asynchronous session
                    try
                    {
                        session = connection.createSession(attributes);

                        // Retrieve and print session ID
                        System.out.println("Session ID:" + session.getId());

                        // Now we will send some messages to our service
                        for (int taskCount = 0; taskCount < tasksToSend; taskCount++)
                        {
                        	// Generate random data size if requested
                        	if (str_data_size.equals("random")) {
                        		Random random = new Random();
                        		data_size = random.nextInt(100000 - 100 + 1) + 100;
                        		spinCpu = data_size;
                        	} else {
                        		data_size = Integer.parseInt(str_data_size);
                        		spinCpu = data_size;
                        	}
                        	
                            // Create a message
                            byte data_value = (byte)taskCount; 
                            SoamDataBlock dataBlock =  new SoamDataBlock(data_size);
                            byte[] data =  dataBlock.buf();
                            for(int i=0;i<data_size; i++)
                            {
                              data[i] = data_value;
                            }
                            
                            MelodyInput myInput = new MelodyInput(taskCount, "InputData", dataBlock, spinCpu);
                            // Set task submission attributes
                            TaskSubmissionAttributes taskAttr = new TaskSubmissionAttributes();
                            taskAttr.setTaskInput(myInput);

                            // Send it
                            TaskInputHandle input = session.sendTaskInput(taskAttr);

                            // Retrieve and print task ID
                            System.out.println("task submitted with ID : " + input.getId());
                            
                            dataBlock.release();
                        }
     
                        synchronized(myCallback)
                        {
                            while (!myCallback.isDone())
                            {
                                myCallback.wait();
                            }
                        }                
                    }
                    finally
                    {
                        // Mandatory session close
                        if (session != null)
                        {
                            session.close();
                            System.out.println("Session closed");
                            System.out.println("connection ID=" + connection.getState());
                        }
                    }
                } catch (Exception exception) {
                	System.out.println("Exception caught in runIt() : ");
                	System.out.println(exception.toString());
                	exception.printStackTrace();
                }
        }
}
