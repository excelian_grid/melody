
package melody.common;

import com.platform.symphony.soam.InputStream;
import com.platform.symphony.soam.Message;
import com.platform.symphony.soam.OutputStream;
import com.platform.symphony.soam.SoamDataBlock;
import com.platform.symphony.soam.SoamException;



/**
 *  This class represents Melody input data object
 *  
 *  @author Grzegorz Walczak
 *  @version 0.1
 * 
 *  */ 

public class MelodyInput extends Message


{
    private int m_id;
    private String m_string;
    private SoamDataBlock m_dataBlock = null;
    private int m_spinCpu;
    private static final long serialVersionUID = 1L;

    public MelodyInput()
    {
        super();
        m_id = 0;
        m_string = null;
        m_dataBlock = new SoamDataBlock();
        m_spinCpu = 0;
    }

    public MelodyInput(int id, String string, SoamDataBlock dataBlock, int spinCpu) throws SoamException
    {
        super();
        m_id = id;
        m_string = string;
        if(m_dataBlock!=null)
        {
          m_dataBlock.release();
        }
        m_dataBlock = dataBlock;
        m_spinCpu = spinCpu;
    }

    
    
    public int getId()
    {
        return m_id;
    }

    public void setId(int id)
    {
        m_id = id;
    }

    public String getString()
    {
        return m_string;
    }
    
    public void setString(String string)
    {
        m_string = string;
    }
    
    public void setData(SoamDataBlock block) throws SoamException
    {
      if(m_dataBlock != null)
      {
        m_dataBlock.release();
      }
      
      m_dataBlock = block;
    }
    
    public SoamDataBlock getData()
    {
      return m_dataBlock;
    }

    public int getSpinCpu()
    {
        return m_spinCpu;
    }

    public void setSpinCpu(int spinCpu)
    {
        m_spinCpu = spinCpu;
    }
    
    public void releaseSoamDataBlock() throws SoamException
    {
      m_dataBlock.release();
      m_dataBlock = null;
    }
    
    @Override
    public void onDeserialize(InputStream stream) throws SoamException {
      m_id = stream.readInt();
      m_string = stream.readString();
      stream.readDataBlock(m_dataBlock);
    }

    @Override
    public void onSerialize(OutputStream stream) throws SoamException {
      stream.writeInt(m_id);
      stream.writeString(m_string);
      stream.writeDataBlock(m_dataBlock);
    }
}