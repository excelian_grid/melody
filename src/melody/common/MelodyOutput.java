
package melody.common;

import com.platform.symphony.soam.InputStream;
import com.platform.symphony.soam.Message;
import com.platform.symphony.soam.OutputStream;
import com.platform.symphony.soam.SoamDataBlock;
import com.platform.symphony.soam.SoamException;

/**
 *  This class represents Melody input data object
 *  
 *  @author Grzegorz Walczak
 *  @version 0.1
 * 
 *  */

public class MelodyOutput extends Message
{
	

    private int m_id;
    private String m_runTime;
    private String m_string;
    private SoamDataBlock m_dataBlock;
    private static final long serialVersionUID = 1L;

    public MelodyOutput()
    {
        super();
        m_id = 0;
        m_dataBlock = new SoamDataBlock();
    }


    public int getId()
    {
        return m_id;
    }

    public void setId(int id)
    {
        m_id = id;
    }

    public String getRunTime()
    {
        return m_runTime;
    }

    public void setRunTime(String runTime)
    {
        m_runTime = runTime;
    }

    public String getString()
    {
        return m_string;
    }
    
    public void setString(String string)
    {
        m_string = string;
    }
    
    public void setData(SoamDataBlock block) throws SoamException
    {
      if(m_dataBlock != null)
      {
        m_dataBlock.release();
      }
      
      m_dataBlock = block;
    }
    
    public SoamDataBlock getData()
    {
      return m_dataBlock;
    }

    public void releaseSoamDataBlock() throws SoamException
    {
      m_dataBlock.release();
      m_dataBlock = null;
    }
    
    @Override
    public void onSerialize(OutputStream stream) throws SoamException {  
      stream.writeInt(m_id);
      stream.writeString(m_string);
      stream.writeString(m_runTime);
      stream.writeDataBlock(m_dataBlock);
    }


    @Override
    public void onDeserialize(InputStream stream) throws SoamException {
      m_id = stream.readInt();
      m_string = stream.readString();
      m_runTime = stream.readString();
      stream.readDataBlock(m_dataBlock);
    }

}