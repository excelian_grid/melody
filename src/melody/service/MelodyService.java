
package melody.service;

import com.platform.symphony.soam.*;
import javax.crypto.*;

import java.util.Date;
import melody.common.*;

public class MelodyService extends ServiceContainer
{
    MelodyService()
    {
        super();
    }


    public void onCreateService(ServiceContext serviceContext) throws SoamException
    {
        /********************************************************************
         * Do your service initialization here. 
         ********************************************************************/
    }

    public void onSessionEnter(SessionContext sessionContext) throws SoamException
    {
        /********************************************************************
         * Do your session-specific initialization here, when common data is
         * provided. 
         ********************************************************************/
    }

    public void onInvoke (TaskContext taskContext) throws SoamException
    {
        /********************************************************************
         * Do your service logic here. This call applies to each task
         * submission. 
         ********************************************************************/

        // Get the input that was sent from the client 
        MelodyInput myInput = new MelodyInput(); 
        taskContext.populateTaskInput(myInput);


        MelodyOutput myOutput = new MelodyOutput();

        // estimate and set our runtime
        Date date = new Date();
        myOutput.setRunTime(date.toString());
        myOutput.setData(myInput.getData());

        // We simply echo the data back to the client 
        myOutput.setId(myInput.getId());
        StringBuffer sb = new StringBuffer();
        
        int spinCpu = myInput.getSpinCpu();
        long start = System.nanoTime();
        
        for (int i = 0; i < 100; i++) 
    	{	
    		spin(spinCpu);
    	}
        sb.append("Client sent : ");
        sb.append(myInput.getString());
        sb.append("\nSymphony replied : Received input. Processed in: " + (System.nanoTime() - start)/1000000 + "ms. Sending output...");
        myOutput.setString(sb.toString());

        // Set our output message 
        taskContext.setTaskOutput(myOutput);
        myInput.releaseSoamDataBlock();
        myOutput.releaseSoamDataBlock();
    }
    
    private static void spin(int spinCpu) {
        long sleepTime = spinCpu*1000L; // convert to nanoseconds
        long startTime = System.nanoTime();
        while ((System.nanoTime() - startTime) < sleepTime) {}
    }

    public void onSessionLeave() throws SoamException
    {
        /********************************************************************
         * Do your session-specific uninitialization here, when common data
         * is provided. 
         ********************************************************************/
    }

    public void onDestroyService() throws SoamException
    {
        /********************************************************************
         * Do your service uninitialization here. 
         ********************************************************************/
    }

    public void onApplicationAttach(ServiceContext serviceContext) throws SoamException
    {
    }

    public void onApplicationDetach() throws SoamException
    {
    }

    // Entry point to the service 
    public static void main(String args[])
    {
        // Return value of our service program 
        int retVal = 0;
        try
        {
            /****************************************************************
             * Do not implement any service initialization before calling the
             * ServiceContainer.run() method. If any service initialization
             * needs to be done, implement the onCreateService() handler for
             * your service container. 
             ****************************************************************/

            // Create the container and run it 
            MelodyService myContainer = new MelodyService();
            myContainer.run();

            /****************************************************************
             * Do not implement any service uninitialization after calling
             * the ServiceContainer.run() method. If any service
             * uninitialization needs to be done, implement the
             * onDestroyService() handler for your service container since
             * there is no guarantee that the remaining code in main() will
             * be executed after calling ServiceContainer::run(). Also, in
             * some cases, the remaining code can even cause an orphan
             * service instance if the code cannot be finished. 
             ****************************************************************/
        }
        catch (Exception ex)
        {
            // Report the exception to stdout 
            System.out.println("Exception caught:");
            System.out.println(ex.toString());
            retVal = -1;
        }


        /********************************************************************
         * NOTE: Although our service program will return an overall failure
         * or success code it will always be ignored in the current revision
         * of the middleware. The value being returned here is for
         * consistency. 
         ********************************************************************/
        System.exit(retVal);
    }
}