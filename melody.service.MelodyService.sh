#!/bin/bash
# Runcom.platform.symphony.samples.SampleApp.service.MyService.sh

###################
# 1. You do not need to modify JAVA_SDK_JARFILE.
# It has already been set by the Symphony Eclipse IDE pluggin. 
###################
JAVA_SDK_JARFILE=$SOAM_HOME/7.1/$EGO_MACHINE_TYPE/lib/JavaSoamApi.jar

###################
# 2. This is where we put your dependent JARs containing all the implementation 
# required for your Application to function.
###################
SOAM_APP_DEP_JARFILES=$SOAM_DEPLOY_DIR/MelodyAppServiceJava.jar

###################
# 3. This is the main class used to start the Application code.
###################
SOAM_SERVICE_MAINCLASS=melody.service.MelodyService

###################
# 4. Additional JVM options are specified here.
###################
JVM_OPTIONS=

###################
# Attention
# You do not need to modify the java start command.
# You can use the appropriate variable instead.
###################
usr/bin/java -classpath $JAVA_SDK_JARFILE:$SOAM_APP_JARFILES:$SOAM_APP_DEP_JARFILES $JVM_OPTIONS $SOAM_SERVICE_MAINCLASS
